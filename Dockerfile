# Establecer la imagen base
FROM node:14-alpine

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar los archivos de la aplicación
COPY package*.json ./

# Instalar las dependencias del proyecto
RUN npm install
# Compilar la aplicación
COPY . .

RUN npm run build

# Comando para iniciar la aplicación cuando el contenedor se ejecute
CMD ["npm", "run" , "start"]
